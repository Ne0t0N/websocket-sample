var ws;

function send() {
    var wsUri = "ws://localhost:8080/websocket-sample-1.0-SNAPSHOT/hello";
    //writeToScreen("Connecting to " + wsUri);

    ws = new WebSocket(wsUri);
    ws.onopen = function (event) {
        //writeToScreen("Connected!");
        var input = document.getElementById("name");
        ws.send(input.value)
    };
    ws.onmessage = function (event) {
        //writeToScreen("Received message: " + event.data);
        writeToScreen(event.data);
        ws.close();
        //writeToScreen("Disconnected!");
    };
    ws.onerror = function (event) {
        writeToScreen('<span style="color: red;">Error: </span>' + event.data)
        ws.close();
        //writeToScreen("Disconnected!");
    };
}

function writeToScreen(message) {
    //var pre = document.createElement("p");
    //pre.innerHTML = message;
    var output = document.getElementById("output");
    output.innerHTML = '<p>' + message + '</p>';
    //output.appendChild(pre);
}