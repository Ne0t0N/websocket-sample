package com.neoton.websocketsample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.websocket.OnMessage;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/hello")
public class HelloServiceEndpoint {

    private static final Logger LOG = LoggerFactory.getLogger(HelloServiceEndpoint.class);

    @OnMessage
    public String hello(String name) {
        LOG.info("Hello service triggered");
        return "Hello, " + name;
    }
}
